/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.colecciones_seed;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase que representa la colección Lista Simple enlazada
 * @author madarme
 */
public class ListaS<T> {
    
    //No hace falta la inicialización (opcional)
    private Nodo<T> cabeza=null;
    private int tamanio=0; //<--Sólo se altera con las operaciones de inserción y/o borrado

    public ListaS() {
    }

    public int getTamanio() {
        return tamanio;
    }
    /**
     * Método adiciona un elemento al inicio de la lista
     * @param datoNuevo objeto a insertar en la lista simple
     */
    
    
    
    public void insertarInicio(T datoNuevo)
    {
    /**
     * Estrategia computacional:
            1. Objeto nuevo en Nodo_nuevo
            2. El sig de Nuevo_nodo es la cabeza
            3. cabeza=Nuevo_nodo
            4. aumentar tamaño
     */
        
        Nodo<T> nuevoNodo=new Nodo(datoNuevo,this.cabeza);
        this.cabeza=nuevoNodo;
        this.tamanio++;
    }

    public void insertarFin(T datoNuevo)
    {
        if(this.esVacia())
            this.insertarInicio(datoNuevo);
        else
        {
        
            
            try {
                Nodo<T> nodoNuevo=new Nodo(datoNuevo, null);
                Nodo<T> nodoUltimo=getPos(this.tamanio-1);
                //Unimos:
                nodoUltimo.setSig(nodoNuevo);
                this.tamanio++;
                
                
            } catch (Exception ex) {
                System.err.println(ex.getMessage());
            }
            
        }
    }
    
    
    public T get(int i)
    {
        
        try {
            return this.getPos(i).getInfo();
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            return null;
        }
    }
    
    
    public void set(int i,T datoNuevo)
    {
        
        try {
            this.getPos(i).setInfo(datoNuevo);
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            
        }
    }
    
    
    /**
     * Método borrar el nodo de la posición i
     * @param i un entero que representa el índice de un nodo
     * @return el objeto almacenado en esa posición
     */
    public T eliminar(int i)
    {
        if(this.esVacia())
            return null;
        //Caso 1: i en cabeza , i==0
        Nodo<T> nodoBorrar=null;
        if(i==0)
        {
            nodoBorrar=this.cabeza;
            this.cabeza=this.cabeza.getSig();
            
        }
        else
        {
            try {
                Nodo<T> nodoAnterior=this.getPos(i-1);
                nodoBorrar=nodoAnterior.getSig();
                //Unir :
                nodoAnterior.setSig(nodoBorrar.getSig());
                
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                return null;
            }
        }
        
        nodoBorrar.setSig(null);
        this.tamanio--;
        return nodoBorrar.getInfo();
    }
    
    
    private Nodo<T> getPos(int i) throws Exception //Excepción obligatoria su tratamiento
    {
        if(i<0 || i>=this.tamanio)
            throw new Exception("índice fuera de rango:"+i);
        
        Nodo<T> nodoPos=this.cabeza;
        while(i>0)
        {
            nodoPos=nodoPos.getSig();
            i--;
        }
        return nodoPos;
    }
    
    @Override
    public String toString() {
        
        if(this.esVacia())
            return "La lista no contiene elementos";
        
        String msg="";
        //vector--> pos en 0 --> hasta length-1
        //Desde la cabeza hasta null
        Nodo<T> nodoActual=this.cabeza;
        while(nodoActual!=null)
        {
            T info=nodoActual.getInfo();
            //Para esto es necesario que la clase en T tenga toString
            msg+=info.toString()+"->";
            nodoActual=nodoActual.getSig();
        }
     return "Cabeza->"+msg+"null";
    }
    
    public boolean esVacia()
    {
        return this.cabeza==null;
    }
    
    /**
     * Elimina todo dato repetido de la lista simple 
     * y deja una sóla incidencia
     * Por ejemplo: L=<3,5,6,3,3,5,7,8> 
     *  L.eliminarRepetidos()--> L=<3,5,6,7,8>
     * 
     * --> 2 primeros ejercicios reciben un bono de 0.5 sobre el P1
     */
    public void eliminarRepetidos()
    {
        //:)
    }
    
    /**
     * Método que permite mover el elemento mayor al final de la lista,
     * el proceso sólo será válido si la lista contiene un ÚNICO elemento mayor
     * Restricciones:
     * 1. Lista no vacía
     * 2. Sólo se puede ejecutar un ciclo
     * 3. No se pueden crear nodos (new Nodo...)
     * 4. No se puede utilizar los métodos adicionar, borrar(eliminar) o getpos
     * @return 
     */
    public boolean moverMayor_al_Fin()
    {
     /**
      * Casos:
            1. L=<>   --> L.moverMayor()->No proceso-Vacía
            2. L=<2,4,56,7> --> L.moverMayor() --> L=<2,4,7,56> return true
            3. L=<2,4,56,7,56> --> L.moverMayor() --> L=<2,4,56,7,56> return false
            4. L=<2> --> L.moverMayor() --> L=<2>  return false
      */   
    //Validación caso 1 y 4
       if(this.esVacia() || this.getTamanio()==1) 
           throw new RuntimeException("No es posible realizar proceso de cambio de mayor");
        
        
       
       Nodo<T> nodoMayor,nodoUltimo,nodoAnt_May; 
       nodoUltimo=nodoMayor=this.cabeza;
       nodoAnt_May=null;
       boolean unMayor=true;
       for(Nodo<T> nodoActual=nodoMayor.getSig();nodoActual!=null;nodoActual=nodoActual.getSig())
       {
           //esto es un error :(
            /**
             * IF NODOACTUAL > NODOMAYOR)
             * NODMAYOR=NODOACTUAL
             */
           int comparador=((Comparable)nodoActual.getInfo()).compareTo(nodoMayor.getInfo());
           if(comparador>0)
           {
               nodoAnt_May=nodoUltimo;
               nodoMayor=nodoActual;
              //L=<2,30,40,30,50,40,1,2,1,2,50,100,2>
               unMayor=true;
                   
           }
           else
           {
                if(comparador==0)
                {
                   unMayor=false; 
                }
           }
           
           
         nodoUltimo=nodoActual;  
       }
       //proceso de desUNir e Unir
       
       if(!unMayor)
           throw new RuntimeException("No es posible realizar proceso de cambio de mayor por que está repetido");
       
       //Esté es el caso del mayor está en el nodoCabeza
       
     if(nodoMayor.getSig()!=null)  
     {    
            if(nodoAnt_May==null)
            {
                this.cabeza=nodoMayor.getSig();

            }
            else
            {
                 nodoAnt_May.setSig(nodoMayor.getSig());

            }
             nodoUltimo.setSig(nodoMayor);
             nodoMayor.setSig(null);
      }

        System.out.println("El mayor es:"+nodoMayor.getInfo().toString());
        return unMayor;
    }
    
    
}
