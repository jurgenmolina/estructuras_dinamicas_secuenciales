/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.colecciones_seed;

/**
 *
 * @author Invitado
 */
public class Cola<T> {
    
    private ListaCD<T> myLista = new ListaCD();

    public Cola() {
    }
    
    public void enColar(T info) {
        this.myLista.insertarFin(info);
    }

    public T deColar() {
        if (this.esVacio()) {
            throw new RuntimeException("Pila vacía");
        }
        return this.myLista.eliminar(0);
    }

    public boolean esVacio() {
        return this.myLista.esVacia();
    }

    public int size() {
        return this.myLista.getTamanio();
    }
    
}
