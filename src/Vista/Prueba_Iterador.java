/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import java.util.Iterator;
import java.util.Random;
import ufps.util.colecciones_seed.ListaCD;

/**
 * EL ITERADOR SE UTILIZA PARA PROCESOS SECUENCIALES
 * @author madarme
 */
public class Prueba_Iterador {

    public static void main(String[] args) {
        problemaEficiencia();
        problemaSolucionado_Iterator1();
        problemaSolucionado_Iterator2();
    }

    /**
     * Esté método usa el iterador a través del foreach
     */
    private static void problemaSolucionado_Iterator1() {
        int n = 1000000;
        ListaCD<Integer> l = crearLista(n);
        int suma = 0;
        for (int dato : l) {
            suma += dato;
        }

        System.out.println("La suma es:" + suma + "\t Con " + n + ", nodos");

    }
    
    
    /**
     * Esté método usa el iterador a través del iterator
     */
    private static void problemaSolucionado_Iterator2() {
        int n = 1000000;
        ListaCD<Integer> l = crearLista(n);
        int suma = 0;
        Iterator<Integer> it=l.iterator();
        while(it.hasNext()) {
            Integer dato=it.next();
            /**
             * dato.getalgio()...
             */
            suma += dato;
        }

        System.out.println("La suma es:" + suma + "\t Con " + n + ", nodos");

    }
    
    

    private static ListaCD<Integer> crearLista(int n) {
        ListaCD<Integer> l = new ListaCD();
        while (n-- > 0) {
            //a partir de: http://chuwiki.chuidiang.org/index.php?title=Generar_n%C3%BAmeros_aleatorios_en_Java
            int valorDado = (int)(Math.floor(Math.random()*n+1));
            l.insertarInicio(valorDado);
        }
        return l;
    }

    private static void problemaEficiencia() {
        int n = 1000;
        ListaCD<Integer> l = crearLista(n);
        //recorrer:
        //Suponer una sumatoria básica
        long suma = 0;
        //Forma a fuerza bruta 
        for (int i = 0; i < l.getTamanio(); i++) {
            suma += l.get(i);
        }

        System.out.println("La suma es:" + suma + "\t Con " + n + ", nodos");
    }
}
